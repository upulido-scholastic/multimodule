package org.zielezin.multiproject.core;

public interface SampleInterface {

  String doStuff();

  void doOtherStuff(String aString);

}
