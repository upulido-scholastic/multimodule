package org.zielezin.multiproject.core;

public class Helper implements SampleInterface {

  public static String getMagicString() {
    String beta = "beta";
    beta.toString();
    return "magic";
  }

  public static double getMagicDouble() {
    Double double1 = new Double(12365d);
    double1.doubleValue();
    System.out.println(double1);
    return 1234d;
  }

  @Override
  public String doStuff() {
    return "Hello World";
  }

  @Override
  public void doOtherStuff(String aString) {

  }
}
