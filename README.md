# Cruson Multi-Module Project Sample

This is a sample [Maven][5] multi module project that can be used as an example how to configure [Cruson][1] plugin to [Fisheye][2]/[Crucible][3].

# [Cruson][1] - A plugin for Atlassian [Fisheye][2]/[Crucible][3] that provides feedback from [Sonar][4] while browsing source code and performing reviews.

## Project preparation:

* Clone the repo to local folder
* Modify pom.xml to match your Sonar configuration
* Run Maven: 'mvn clean install sonar:sonar'
* You should be able to see the project on Sonar dashboard

## Crucible/Fisheye configuration:

* Go to Crucible/FishEye Administration panels
* Configure global Cruson settings via 'Cruson settings' link in general section.

		E.G.
		Sonar url - http://localhost:9000/
		Default Source Folders - src/main/java/,src/test/java/
		Default extensions - java

* Create new repository with name 'cruson-multiproject-sample'
* Wait till it gets indexed
* In repository 'Actions' choose repo configuration and set:

		GroupId - org.zielezin.cruson
		ArtifactId - cruson-multiproject-sample
		MultiModule - true
		Multi Module Artifacts - someCore,someModule
		
* Browse source and you should see additional information next to lines with issues identified by Sonar

# License

Copyright 2013 Piotr Zielezinski

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

<http://www.apache.org/licenses/LICENSE-2.0>

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

[1]: https://bitbucket.org/Zielezin/cruson
[2]: http://www.atlassian.com/software/fisheye/overview
[3]: http://www.atlassian.com/software/crucible/overview
[4]: http://www.sonarsource.org/
[5]: http://maven.apache.org/