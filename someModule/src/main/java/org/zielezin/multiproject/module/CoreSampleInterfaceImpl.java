package org.zielezin.multiproject.module;

import org.zielezin.multiproject.core.SampleInterface;

public class CoreSampleInterfaceImpl implements SampleInterface {

  @Override
  public String doStuff() {
    char[] charArray = "Soasda not used string".toCharArray();
    return "some String";
  }

  @Override
  public void doOtherStuff(String aString) {
    // TODO Auto-generated method stub

  }

}
