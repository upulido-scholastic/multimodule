package org.zielezin.multiproject.module;

import org.junit.Test;

import static org.junit.Assert.*;

public class CoreSampleInterfaceImplTest {

  @Test
  public void testImpl() {
    CoreSampleInterfaceImpl coreSampleInterfaceImpl = new CoreSampleInterfaceImpl();
    String doStuff = coreSampleInterfaceImpl.doStuff();
    String a1 = "some String";

    String a3 = "some String";
    String a2 = "some String";

    String sum = a1 + a2 + a3;
    assertEquals("some String", doStuff);
  }

}
